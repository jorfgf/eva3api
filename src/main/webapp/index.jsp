<%-- 
    Document   : index
    Created on : 10-06-2021, 20:23:16
    Author     : Ripley
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Api Hoteles</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>
    <body>
        
        <div class="container">
            
            <div class="row">
                <div class="col-md-6">
                     <div class="card mt-2">
                        <div class="card-header">
                            Nombre: Jose Era <br>
                            Rut: 27454293-4 <br>
                            Seccion : SECCIÓN 6 <br>
                            Descripcion: Evaluacion3 Aplicaciones Empresariales <br><br>
                            <hr>
                            Endpoints Api Hoteles
                        </div>
                        <div class="card-body">
                           <ul class="list-group">
                            <li class="list-group-item">Lista Hoteles - GET - https://ciisaapieva3.herokuapp.com/api/hoteles </li>
                            <li class="list-group-item">Lista Hoteles por id - GET - https://ciisaapieva3.herokuapp.com/api/hoteles/{id}</li>
                            <li class="list-group-item">Crear Hoteles - POST - https://ciisaapieva3.herokuapp.com/api/hoteles</li>
                            <li class="list-group-item">Actualizar Hoteles - PUT - https://ciisaapieva3.herokuapp.com/api/hoteles</li>
                            <li class="list-group-item">Eliminar Hoteles - DELETE - https://ciisaapieva3.herokuapp.com/api/hoteles/{id}</li>
                          </ul>
                        </div>
                    </div>

                </div>
            </div>
           
        </div>
       
         
    </body>
</html>
