/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eva3.services;

import com.mycompany.eva3.dao.HotelesJpaController;
import com.mycompany.eva3.dao.exceptions.NonexistentEntityException;
import com.mycompany.eva3.entity.Hoteles;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author joseera
 */
@Path("hoteles")
public class HotelesRest {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarHoteles(){

        HotelesJpaController dao=new HotelesJpaController();

      List<Hoteles> hoteles=  dao.findHotelesEntities();

      return Response.ok(200).entity(hoteles).build();


    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public  Response crear(Hoteles hoteles ){

       HotelesJpaController dao=new HotelesJpaController();  
        try {
            dao.create(hoteles);
        } catch (Exception ex) {
            Logger.getLogger(HotelesRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(hoteles).build();


    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Hoteles hoteles ){

       HotelesJpaController dao=new HotelesJpaController();  

        try {
            dao.edit(hoteles);
        } catch (Exception ex) {
            Logger.getLogger(HotelesRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(hoteles).build();

    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
            
    public Response eliminar(@PathParam("id") String id){

         
        HotelesJpaController dao=new HotelesJpaController();  
           
        try {
            dao.destroy(Long.parseLong(id));
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(HotelesRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("Hotel eliminado").build();


    }
    
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorId(@PathParam("id") String id){

         HotelesJpaController dao=new HotelesJpaController();  
         Hoteles hotel = dao.findHoteles(Long.parseLong(id));

        return Response.ok(200).entity(hotel).build();


    }

}
