/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.eva3.dao;

import com.mycompany.eva3.dao.exceptions.NonexistentEntityException;
import com.mycompany.eva3.dao.exceptions.PreexistingEntityException;
import com.mycompany.eva3.entity.Hoteles;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author joseera
 */
public class HotelesJpaController implements Serializable {

    public HotelesJpaController() {
    }
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("hoteles_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Hoteles hoteles) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(hoteles);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findHoteles(hoteles.getId()) != null) {
                throw new PreexistingEntityException("Hoteles " + hoteles + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Hoteles hoteles) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            hoteles = em.merge(hoteles);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = hoteles.getId();
                if (findHoteles(id) == null) {
                    throw new NonexistentEntityException("The hoteles with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Hoteles hoteles;
            try {
                hoteles = em.getReference(Hoteles.class, id);
                hoteles.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hoteles with id " + id + " no longer exists.", enfe);
            }
            em.remove(hoteles);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Hoteles> findHotelesEntities() {
        return findHotelesEntities(true, -1, -1);
    }

    public List<Hoteles> findHotelesEntities(int maxResults, int firstResult) {
        return findHotelesEntities(false, maxResults, firstResult);
    }

    private List<Hoteles> findHotelesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Hoteles.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Hoteles findHoteles(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Hoteles.class, id);
        } finally {
            em.close();
        }
    }

    public int getHotelesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Hoteles> rt = cq.from(Hoteles.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
